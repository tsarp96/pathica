/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Row } from 'antd';
import pathicaLogo from './pathica_logo.png';

// Css
import s from './Header.css';
// Components
import Navigation from '../Navigation';
import NavRight from '../NavRight/NavRight';
import SearchBox from '../SearchBox';
// import Link from '../Link';

class Header extends React.Component {
  render() {
    return (
      <div className={s.Header}>
        <Row>
          <div className={s.Brand_Logo}>
            <img src={pathicaLogo} alt="pathicalogo" />
          </div>
          <Navigation />
          <div className={s.HeaderRight}>
            <SearchBox />
            <NavRight />
          </div>
        </Row>
      </div>
    );
  }
}

export default withStyles(s)(Header);
